import axios from 'axios';

const url = process.env.REACT_APP_BASE_URL || 'http://localhost:5000/api';

export const api = {
  getOrders: async () => {
    try {
      const response = await axios({
        url: `${url}/orders`,
        method: 'GET',
      });
      return response.data;
    } catch (error) {
      throw new Error(error);
    }
  },

  getOrderById: async (id) => {
    try {
      const response = await axios({
        url: `${url}/orders/${id}`,
        method: 'GET',
      });
      return response.data;
    } catch (error) {
      throw new Error(error);
    }
  },

  addOrder: async (data) => {
    try {
      const response = await axios({
        url: `${url}/orders/`,
        method: 'POST',
        data,
      });
      return response.data;
    } catch (error) {
      throw new Error(error);
    }
  },

  cancelOrder: async (id) => {
    try {
      const response = await axios({
        url: `${url}/orders/${id}`,
        method: 'PUT',
        data: {
          status: 'cancelled',
        },
      });
      return response.data;
    } catch (error) {
      throw new Error(error);
    }
  },
};
