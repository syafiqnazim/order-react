import { toast } from 'react-toastify';

export const errorHandler = (message) => {
  typeof message === 'string'
    ? toast.error(message)
    : toast.error('Something went wrong. Please try again.');
};

export const successHandler = (message) => {
  typeof message === 'string'
    ? toast.success(message)
    : toast.success('Success!');
};
