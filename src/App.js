import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Homepage from '@pages/homepage';
import CreateOrder from '@pages/createOrder';
import OrderDetails from '@pages/orderDetails';

const App = () => {
  const routes = [
    {
      path: '/',
      component: Homepage,
    },
    {
      path: '/orders/new',
      component: CreateOrder,
    },
    {
      path: '/order/:id',
      component: OrderDetails,
    },
  ];
  return (
    <BrowserRouter>
      <ToastContainer autoClose={5000} />
      <Switch>
        {routes.map((route) => (
          <Route exact path={route.path} component={route.component} />
        ))}
      </Switch>
    </BrowserRouter>
  );
};

export default App;
