import React from 'react';
import {
  AppBar, Toolbar, Typography, makeStyles, Container,
} from '@material-ui/core';

const useStyles = makeStyles(() => ({
  appBar: {
    backgroundColor: '#D60265',
  },
}));

const navbar = () => {
  const classes = useStyles();

  return (
    <AppBar position="static" className={classes.appBar}>
      <Toolbar>
        <Container maxWidth="lg">
          <Typography variant="h6" className={classes.title}>
            Food Tiger&apos;s Order Application
          </Typography>
        </Container>
      </Toolbar>
    </AppBar>
  );
};

export default navbar;
