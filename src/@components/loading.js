import React from 'react';
import { makeStyles, CircularProgress } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  screen: {
    height: '100vh',
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

const Loading = () => {
  const classes = useStyles();
  return (
    <div className={classes.screen}>
      <CircularProgress size={70} color="secondary" />
    </div>
  );
};

export default Loading;
