import React, { useState, useEffect } from 'react';
import {
  Container, Typography, Breadcrumbs, makeStyles,
  Paper, CssBaseline, Table, TableContainer, TableHead,
  TableRow, TableCell, TableBody, Button,
} from '@material-ui/core';
import moment from 'moment';
import _ from 'lodash';

import Navbar from '@components/navbar';
import Loading from '@components/loading';
import { errorHandler } from '@utils/handlers';
import { api } from '../@api/api';

const useStyles = makeStyles(() => ({
  breadCrumbs: {
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    backgroundColor: 'white',
    padding: 50,
  },
  table: {
    minWidth: 650,
  },
  button: {
    marginTop: 30,
    marginBottom: 30,
  },
}));

const Homepage = () => {
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(false);
  const [ordersData, setOrdersData] = useState();

  const statusColor = {
    created: 'black',
    confirmed: 'green',
    cancelled: 'red',
    delivered: 'blue',
  };

  const getOrders = async () => {
    setIsLoading(true);
    try {
      const response = await api.getOrders();
      if (response) {
        setOrdersData(_.sortBy(response, ['updatedAt']).reverse());
        setIsLoading(false);
      }
    } catch (error) {
      errorHandler(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getOrders();
  }, []);

  return (
    <>
      {isLoading ? <Loading />
        : (
          <>
            <CssBaseline />
            <Navbar />
            <Container maxWidth="lg" className={classes.container}>
              <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumbs}>
                <Typography color="textPrimary">Homepage</Typography>
              </Breadcrumbs>
              <Button
                color="secondary"
                variant="outlined"
                className={classes.button}
                href="/orders/new"
              >
                Add New Order
              </Button>
              <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>Order&apos;s ID</TableCell>
                      <TableCell>Details</TableCell>
                      <TableCell>Created On</TableCell>
                      <TableCell>Last Modified</TableCell>
                      <TableCell>Status</TableCell>
                      <TableCell>Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {ordersData?.map((order) => (
                      <TableRow key={order._id}>
                        <TableCell component="th" scope="row">{order._id}</TableCell>
                        <TableCell component="th" scope="row">{order.details}</TableCell>
                        <TableCell>{moment(order?.createdAt).fromNow()}</TableCell>
                        <TableCell>{moment(order?.updatedAt).fromNow()}</TableCell>
                        <TableCell style={{ color: statusColor[order.status], textTransform: 'capitalize' }}>
                          {order.status}
                        </TableCell>
                        <TableCell>
                          <Button color="primary" variant="outlined" href={`/order/${order._id}`}>
                            View Order
                          </Button>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Container>
          </>
        )}
    </>
  );
};

export default Homepage;
