import React, { useState } from 'react';
import {
  Container, Typography, Breadcrumbs, makeStyles, Paper, CssBaseline, Grid, Button, Link, TextField,
} from '@material-ui/core';
import { useHistory } from 'react-router-dom';

import Navbar from '@components/navbar';
import Loading from '@components/loading';
import { successHandler, errorHandler } from '@utils/handlers';
import { api } from '../@api/api';

const useStyles = makeStyles(() => ({
  breadCrumbs: {
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    backgroundColor: 'white',
    padding: 50,
  },
  table: {
    minWidth: 650,
  },
  paper: {
    padding: 50,
  },
  button: {
    marginTop: 50,
    marginRight: 10,
  },
  gridContainer: {
    alignItems: 'center',
  },
}));

const CreateOrder = () => {
  const classes = useStyles();
  const history = useHistory();

  const [isLoading, setIsLoading] = useState(false);
  const [orderDetails, setOrderDetails] = useState();

  const addOrder = async () => {
    if (!orderDetails) {
      errorHandler('Please fill in the details.');
      return;
    }
    setIsLoading(true);
    try {
      const data = { details: orderDetails };
      const response = await api.addOrder(data);
      if (response) {
        successHandler(`Order id ${response._id} created`);
        setIsLoading(false);
        history.push('/');
      }
    } catch (error) {
      errorHandler(error);
      setIsLoading(false);
    }
  };

  return (
    <>
      {isLoading ? <Loading />
        : (
          <>
            <CssBaseline />
            <Navbar />
            <Container maxWidth="lg" className={classes.container}>
              <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumbs}>
                <Link color="primary" href="/">
                  Homepage
                </Link>
                <Typography color="textPrimary">Create New Order</Typography>
              </Breadcrumbs>
              <Paper className={classes.paper}>
                <Grid container spacing={3} className={classes.gridContainer}>
                  <Grid item xs={2}>
                    <Typography>Order Details:</Typography>
                  </Grid>
                  <Grid item xs={10}>
                    <TextField
                      required
                      id="outlined-required"
                      label="Required"
                      variant="outlined"
                      onChange={(e) => setOrderDetails(e.target.value)}
                    />
                  </Grid>
                </Grid>
                <Button color="primary" variant="outlined" className={classes.button} onClick={() => addOrder()}>
                  Add Order
                </Button>
                <Button color="secondary" variant="outlined" className={classes.button} href="/">Cancel</Button>
              </Paper>
            </Container>
          </>
        )}
    </>
  );
};
export default CreateOrder;
