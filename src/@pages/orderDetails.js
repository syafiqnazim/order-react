import React, { useState, useEffect } from 'react';
import {
  Container, Typography, Breadcrumbs, makeStyles, Paper, CssBaseline, Grid, Button, Link,
} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import moment from 'moment';

import Navbar from '@components/navbar';
import Loading from '@components/loading';
import { successHandler, errorHandler } from '@utils/handlers';
import { api } from '../@api/api';

const useStyles = makeStyles(() => ({
  breadCrumbs: {
    marginTop: 10,
    marginBottom: 10,
  },
  container: {
    backgroundColor: 'white',
    padding: 50,
  },
  table: {
    minWidth: 650,
  },
  paper: {
    padding: 50,
  },
  button: {
    marginTop: 30,
    marginRight: 20,
  },
}));

const OrderDetails = () => {
  const classes = useStyles();
  const { id } = useParams();

  const [isLoading, setIsLoading] = useState(false);
  const [orderData, setOrderData] = useState();

  const statusColor = {
    created: 'black',
    confirmed: 'green',
    cancelled: 'red',
    delivered: 'blue',
  };

  const getOrderById = async () => {
    setIsLoading(true);
    try {
      const response = await api.getOrderById(id);
      if (response) {
        setOrderData(response);
        setIsLoading(false);
      }
    } catch (error) {
      setIsLoading(false);
      errorHandler(error);
    }
  };

  const cancelOrder = async () => {
    setIsLoading(true);
    try {
      const response = await api.cancelOrder(id);
      if (response) {
        setOrderData(response);
        successHandler('Order Cancelled');
        setIsLoading(false);
      }
    } catch (error) {
      setIsLoading(false);
      errorHandler(error);
    }
  };

  useEffect(() => {
    getOrderById();
  }, []);

  return (
    <>
      {isLoading ? <Loading />
        : (
          <>
            <CssBaseline />
            <Navbar />
            <Container maxWidth="lg" className={classes.container}>
              <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumbs}>
                <Link color="primary" href="/">
                  Homepage
                </Link>
                <Typography color="textPrimary">{`Order Id - ${id}`}</Typography>
              </Breadcrumbs>
              <Paper className={classes.paper}>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Typography>ID:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>{id}</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>Details:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>{orderData?.details}</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>Status:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography style={{ color: statusColor[orderData?.status], textTransform: 'capitalize' }}>
                      {orderData?.status}
                    </Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>Created On:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>{moment(orderData?.createdAt).format('DD-MM-YY (hh:mm:ssa)')}</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>Last Modified:</Typography>
                  </Grid>
                  <Grid item xs={6}>
                    <Typography>{moment(orderData?.updatedAt).format('DD-MM-YY (hh:mm:ssa)')}</Typography>
                  </Grid>
                </Grid>
                <Button color="secondary" variant="outlined" className={classes.button} onClick={() => cancelOrder()}>
                  Cancel Order
                </Button>
                <Button color="primary" variant="outlined" className={classes.button} href="/">
                  Back
                </Button>
              </Paper>
            </Container>
          </>
        )}
    </>
  );
};

export default OrderDetails;
