# Food Tiger's Order Application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## This is a front-end application for an order microservice

### You can find the backend repositories as below:

 - https://gitlab.com/syafiqnazim/order-app
 - https://gitlab.com/syafiqnazim/order-payment

In the project directory, you can run:

 ### `yarn start ` or `npm start ` to start the application but you need to have the .env file.

 ### Please do let me know if you would like to have the .env file.

Content
===

### 1. Homepage
 See all orders details that have been made.

### 2. Order's Details
 See one order's details by id.
 There's a button to cancel the order.



### 3. Add New
 Create new order with input given.

 Contact
 ===
 Please let me know if there's a bug in this application at:
 https://gitlab.com/syafiqnazim
